﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoD
{
    internal class Project
    {
        public string id { get; set; }

        private string number, condition, equipment;
        private int time, temperature;
        //private float failure_rate;

        public string Number
        {
            get { return number; }
            set { number = value; }
        }
        public string Condition
        {
            get { return condition; }
            set { condition = value; }
        }
        public string Equipment
        {
            get { return equipment; }
            set { equipment = value; }
        }
        public int Time
        {
            get { return time; }
            set { time = value; }
        }
        public int Temperature
        {
            get { return temperature; }
            set { temperature = value; }
        }
        //public float FailureRate
        //{
        //    get { return failure_rate; }
        //    set { failure_rate = value; }
        //}


        public Project() { }

        public Project(string id, string number, string condition, string equipment, int time, int temperature)
        {
            this.id = id;
            this.number = number;
            this.condition = condition;
            this.equipment = equipment;
            this.time = time;
            this.temperature = temperature;
            //this.failure_rate = failure_rate;
        }
    }
}
