﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoD
{
    class Component
    {
        public int id { get; set; }
        public string project_id { get; set; }
        public string name { get; set; }
        private int sample, multiplicity, total_time, total_time_redundancy;
        private float failure_rate, failure_rate_storage;
        private string type, nominal, redundancy;

        public int Sample
        {
            get { return sample; }
            set { sample = value; }
        }
        public float Failure_rate
        {
            get { return failure_rate; }
            set { failure_rate = value; }
        }

        public float Failure_rate_storage
        {
            get { return failure_rate_storage; }
            set { failure_rate_storage = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public string Nominal
        {
            get { return nominal; }
            set { nominal = value; }
        }

        public string Redundancy
        {
            get { return redundancy; }
            set { redundancy = value; }
        }
        public int Multiplicity
        {
            get { return multiplicity; }
            set { multiplicity = value; }
        }

        public int Total_Time
        {
            get { return total_time; }
            set { total_time = value; }
        }

        public int Total_Time_Redundancy
        {
            get { return total_time_redundancy;}
            set { total_time_redundancy = value;}
        }

        public Component() { }
        public Component(int sample, float failure_rate, float failure_rate_storage, string type, string nominal, string name, string project_id, string redundancy, int multiplicity, int total_time, int total_time_redundancy) 
        {
            this.sample = sample;
            this.failure_rate = failure_rate;
            this.failure_rate_storage = failure_rate_storage;
            this.type = type;
            this.nominal = nominal;
            this.name = name;
            this.project_id = project_id;
            this.redundancy = redundancy;
            this.multiplicity = multiplicity;
            this.total_time = total_time;
            this.total_time_redundancy = total_time_redundancy;
        }
    }
}
