﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;
using System.IO.Ports;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Color = System.Windows.Media.Color;
using DocumentFormat.OpenXml.Math;
using Control = System.Windows.Controls.Control;
using Style = System.Windows.Style;
using DocumentFormat.OpenXml.Office2010.Excel;
using System.Data.SQLite;
using System.Windows.Media.Animation;
using System.Numerics;

using LiveCharts;
using LiveCharts.Wpf;
using LiveChartsCore.SkiaSharpView.Painting;
using SkiaSharp;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using LiveCharts.Helpers;
using System.Runtime.Serialization;
using DocumentFormat.OpenXml.Vml.Spreadsheet;
using DocumentFormat.OpenXml.Presentation;

namespace AoD
{
    public partial class ProjectWindow : Window, INotifyPropertyChanged
    {
        ApplicationContext db;
        public static ProjectWindow Instance;
        public string CurProject { get; set; }
        public string CurComponent { get; set; }

        

        //Project Graph
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<string, string> Values { get; set; }

        //Component Graph
        public SeriesCollection SeriesCollectionComponent { get; set; }
        public string[] LabelsComponent { get; set; }
        public Func<double, string> Formatter { get; set; }

        public ProjectWindow(string curProject)
        {
            InitializeComponent();
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
            db = new ApplicationContext();
            CurProject = curProject;

            //Project
            List<Project> projects = db.Projects.ToList();
            foreach (Project project in projects)
                if (project.id == curProject)
                {
                    Button_Project.Content = curProject;
                    TextProjectName.Text = curProject;
                    textDecimalNumber.Text = project.Number.ToString();
                    textCondition.Text = project.Condition.ToString();
                    textEquipment.Text = project.Equipment.ToString();
                    textTemperature.Text = project.Temperature.ToString();
                    textTotalTime.Text = project.Time.ToString();
                }

            List<Component> components = db.Components.ToList();
            List<string> listId = new List<string>();
            List<int> listTotalTime = new List<int>();

            double system_failure_rate = 0;

            foreach (Component component in components)
                if (component.project_id == curProject)
                {
                    listId.Add(component.name);
                    listTotalTime.Add(component.Total_Time_Redundancy);
                    system_failure_rate = system_failure_rate + component.Failure_rate;
                }

            textProjectFailureRate.Text = Math.Round(system_failure_rate, 9).ToString();

            SeriesCollection = new SeriesCollection()
            {
                new ColumnSeries
                {
                    Title = "Tн.м = ",
                    Values = new ChartValues<int> { }
                }
            };

            SeriesCollectionComponent = new SeriesCollection()
            {
                new RowSeries
                {
                    Title = "Tн.м = ",
                    Values = new ChartValues<int> { }
                }
            };

            for (int i = 0; i< listId.Count; i++)
            {
                if (listTotalTime[i] > Int32.Parse(textTotalTime.Text))
                    listTotalTime[i] = Int32.Parse(textTotalTime.Text);
                    
                SeriesCollection[0].Values.Add(listTotalTime[i]);
            }

            AxisY.MaxValue = Double.Parse(textTotalTime.Text);
            AxisX.MaxValue = Double.Parse(textTotalTime.Text);
            
            Labels = listId.ToArray();
            Values = value => value.ToString();

            ComponentAxisY.Labels = new[] { CurComponent };
            //LabelsComponent = new[] { "gfg" };

            DataContext = this;


            Button[] buttons = new Button[listId.Count];
            Button[] delbuttons = new Button[listId.Count];

            for (int i = 0; i < listId.Count; i++)
            {
                buttons[i] = new Button();
                buttons[i].MinWidth = 150;
                buttons[i].Height = 40;
                buttons[i].HorizontalAlignment = HorizontalAlignment.Center;
                buttons[i].Margin = new Thickness(30, 0, 0, 10);
                buttons[i].BorderBrush = Brushes.Transparent;
                buttons[i].Background = Brushes.Transparent;
                buttons[i].Content = (listId[i]).ToString();
                buttons[i].Click += new RoutedEventHandler(Button_Component_Click);
                string b = listId[i].ToString();

                delbuttons[i] = new Button();
                delbuttons[i].Width = 30;
                delbuttons[i].HorizontalAlignment = HorizontalAlignment.Center;
                delbuttons[i].Margin = new Thickness(0, 5, 0, 12);
                delbuttons[i].Content = (listId[i]).ToString(); //
                delbuttons[i].Click += new RoutedEventHandler(Button_Component_Delete_Click);
                delbuttons[i].BorderThickness = new Thickness(0);
                delbuttons[i].Background = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/Pictures/bin.png")));

                wrapPanel.Children.Add(buttons[i]);
                wrapPanel.Children.Add(delbuttons[i]);
            }
        }

        private double factorial(int value)
        {
            var factorial = new BigInteger(1);
            for (int i = 1; i <=value; i++)
            {
                factorial *= i;
            }
            return (double)factorial;
        }

        private void Button_Project_Click(object sender, RoutedEventArgs e)
        {
            ProjectGrid.Visibility = Visibility.Visible;
            CreationComponentGrid.Visibility = Visibility.Collapsed;
            ComponentGrid.Visibility = Visibility.Collapsed;
            RedundancyGrid.Visibility = Visibility.Collapsed;

            List<Component> components = db.Components.ToList();
            List<string> listId = new List<string>();
            List<int> listTotalTime = new List<int>();

            foreach (Component component in components)
                if (component.project_id == CurProject)
                {
                    listId.Add(component.name);
                    listTotalTime.Add(component.Total_Time_Redundancy);
                }

            SeriesCollection[0].Values.Clear();

            for (int i = 0; i < listId.Count; i++)
            {
                if (listTotalTime[i] > Int32.Parse(textTotalTime.Text))
                    listTotalTime[i] = Int32.Parse(textTotalTime.Text);

                SeriesCollection[0].Values.Add(listTotalTime[i]);
            }


            Labels = listId.ToArray();
            Values = value => value.ToString();


            DataContext = this;

        }
        private void Button_Component_Click(object sender, RoutedEventArgs e)
        {
            ComponentGrid.Visibility = Visibility.Visible;
            CreationComponentGrid.Visibility = Visibility.Collapsed;
            ProjectGrid.Visibility = Visibility.Collapsed;
            RedundancyGrid.Visibility = Visibility.Collapsed;

            var button = (Button)sender;

            string componentName = (string)button.Content;

            List<Component> components = db.Components.ToList();
            string projectId = CurProject;
            foreach (Component component in components)
                if (component.project_id == projectId && component.name == componentName)
                {
                    CurComponent = componentName;
                    Button_Project.Content = projectId;
                    TextBlockNominal.Text = component.Nominal.ToString();
                    TextType.Text = component.Type.ToString();
                    TextName.Text = component.name.ToString();
                    TextFailureRate.Text = component.Failure_rate.ToString();
                    TextFailureRateStorage.Text = component.Failure_rate_storage.ToString();
                    TextSample.Text = component.Sample.ToString();
                    TextMultiplicity.Text = component.Multiplicity.ToString();
                    RedundancyBox.Text = component.Redundancy;

                    double quantile = 0;
                    double gamma = (100 * component.Sample - 100) / component.Sample;
                    double gamma_percentile_life = -(Math.Log(gamma / 100) / component.Failure_rate);
                    double gamma_round = Math.Round(gamma);
                    double gamma_round1 = Math.Round(gamma, 1);
                    double gamma_round2 = Math.Round(gamma, 2);
                    var gamma_quantiles = new Dictionary<double, double>()
                    {
                        { 90, 1.282 },
                        { 91, 1.341 },
                        { 92, 1.405 },
                        { 93, 1.476 },
                        { 94, 1.555 },
                        { 95, 1.645 },
                        { 96, 1.751 },
                        { 97, 1.881 },
                        { 98, 2.054 },
                        { 99, 2.326 },
                        { 99.9, 3.090 },
                        { 99.99, 3.719 }
                    };

                    foreach (var gamma_quantile in gamma_quantiles)
                        if (gamma_quantile.Key == gamma_round)
                            quantile = gamma_quantile.Value;

                    foreach (var gamma_quantile in gamma_quantiles)
                        if (gamma_quantile.Key > gamma_round1 && gamma_round1 > 99)
                            quantile = gamma_quantile.Value;

                    foreach (var gamma_quantile in gamma_quantiles)
                        if (gamma_quantile.Key > gamma_round2 && gamma_round2 > 99.9)
                            quantile = gamma_quantile.Value;

                    TextTotalTime.Text = Convert.ToString(Math.Round((gamma_percentile_life * (1 - 0.15 * 3.090)) / (1 - 0.15 * quantile)));
                    component.Total_Time = int.Parse(TextTotalTime.Text);
                    db.SaveChanges();


                    TextTotalTimeRedundancy.Text = Convert.ToString(component.Total_Time_Redundancy);

                    if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                        ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                    else
                        ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));

                    if (component.Redundancy == "Без резервирования")
                    {
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/No.png"));
                        TextTotalTimeRedundancy.Text = TextTotalTime.Text;
                        component.Total_Time_Redundancy = Convert.ToInt32(TextTotalTime.Text);
                        db.SaveChanges();
                    }

                    else if (component.Redundancy == "Нагруженный")
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Loaded.png"));
                    else if (component.Redundancy == "Облегченный")
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Light.png"));
                    else if (component.Redundancy == "Ненагруженный")
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Unloaded.png"));
                    else
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/noPhoto.png"));

                    if (component.Nominal == "IR2153")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/IR2153.jpg"));
                    else if (component.Nominal == "2N5401")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/2N5401.jpg"));
                    else if (component.Nominal == "2N5551")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/2N5551.jpg"));
                    else if (component.Nominal == "C1-4")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/C1-4.jpg"));
                    else if (component.Nominal == "FR607 x 4")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/FR607.jpg"));
                    else if (component.Nominal == "HER108")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/HER108.jpg"));
                    else if (component.Nominal == "IRF740")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/IRF740.jpg"));
                    else if (component.Nominal == "K10-17A")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/K10-17A.jpg"));
                    else if (component.Nominal == "K50-35")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/K50-35.jpeg"));
                    else if (component.Nominal == "K73-17")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/K73-17.jpg"));
                    else if (component.Nominal == "L15KC")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/L15KC.jpg"));
                    else if (component.Nominal == "L-132XIT")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/L-132XIT.jpg"));
                    else if (component.Nominal == "MIT-V")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/MIT-V.jpg"));
                    else if (component.Nominal == "PLA10")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/PLA10.jpg"));
                    else if (component.Nominal == "RS405L")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/RS405L.jpg"));
                    else
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/NoPhoto.jpg"));


                    int valueTime = int.Parse(textTotalTime.Text);
                    SeriesCollectionComponent[0].Values.Clear();
                    if (component.Total_Time_Redundancy > valueTime)
                        SeriesCollectionComponent[0].Values.Add(valueTime);
                    else
                        SeriesCollectionComponent[0].Values.Add(component.Total_Time_Redundancy);
                    ComponentAxisY.Labels = new[] { CurComponent };
                    Formatter = value => value.ToString();

                }
        }
        private void TextMultiplicity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem multiplicity = ((sender as ComboBox).SelectedItem as ComboBoxItem);
            Component component = new Component();
            component = db.Components.Where(c => c.name == CurComponent && c.project_id == CurProject).Single();
            component.Multiplicity = Int32.Parse(multiplicity.Content.ToString());
            db.SaveChanges();

            if (component.Redundancy == "Без резервирования")
            {
                RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/No.png"));
                TextTotalTimeRedundancy.Text = TextTotalTime.Text;
                component.Total_Time_Redundancy = component.Total_Time;
                db.SaveChanges();

                if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                else
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));
            }

            else if (component.Redundancy == "Нагруженный")
            {
                RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Loaded.png"));
                double failureRate = Double.Parse(TextFailureRate.Text);
                int m = component.Multiplicity;
                double t = 100;

                double probability = 1 - Math.Pow(1 - Math.Pow(Math.E, (-1) * failureRate * t), (m + 1));
                while (probability > 0.999)
                {
                    t = t + 100;
                    probability = 1 - Math.Pow(1 - Math.Pow(Math.E, (-1) * failureRate * t), (m + 1));
                }

                TextTotalTimeRedundancy.Text = Convert.ToString(t);

                component.Total_Time_Redundancy = (int)t;
                db.SaveChanges();

                if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                else
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));
            }

            else if (component.Redundancy == "Облегченный")
            {
                RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Light.png"));
                double failureRate = Double.Parse(TextFailureRate.Text);
                double failureRateStorage = Double.Parse(TextFailureRateStorage.Text);
                int m = component.Multiplicity;
                double t = 100;
                double alpha = failureRateStorage / failureRate;

                double probability = Math.Pow(Math.E, (-1) * failureRate * t) * (1 + (1 / alpha) * (1 - Math.Pow(Math.E, (-1) * alpha * failureRate * t)));

                while (probability > 0.999)
                {
                    t = t + 100;
                    probability = Math.Pow(Math.E, (-1) * failureRate * t) * (1 + (1 / alpha) * (1 - Math.Pow(Math.E, (-1) * alpha * failureRate * t)));
                }

                TextTotalTimeRedundancy.Text = Convert.ToString(t);

                component.Total_Time_Redundancy = (int)t;
                db.SaveChanges();

                if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                else
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));
            }

            else if (component.Redundancy == "Ненагруженный")
            {
                RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Unloaded.png"));
                double failureRate = Double.Parse(TextFailureRate.Text);
                double failureRateStorage = Double.Parse(TextFailureRateStorage.Text);
                int m = component.Multiplicity;
                double t = 100;
                double probability = 0;

                for (int j = 0; j < m + 1; j++)
                {
                    probability = probability + (Math.Pow(failureRate * t, j) / factorial(j));
                }

                probability = probability * Math.Pow(Math.E, (-1) * failureRate * t);

                while (probability > 0.999)
                {
                    probability = 0;
                    t = t + 100;
                    for (int j = 0; j < m + 1; j++)
                    {
                        probability = probability + (Math.Pow(failureRate * t, j) / factorial(j));
                    }

                    probability = probability * Math.Pow(Math.E, (-1) * failureRate * t);
                }

                TextTotalTimeRedundancy.Text = Convert.ToString(t);

                component.Total_Time_Redundancy = (int)t;
                db.SaveChanges();

                if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                else
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));
            }


            int valueTime = int.Parse(textTotalTime.Text);
            SeriesCollectionComponent[0].Values.Clear();
            if (component.Total_Time_Redundancy > valueTime)
                SeriesCollectionComponent[0].Values.Add(valueTime);
            else
                SeriesCollectionComponent[0].Values.Add(component.Total_Time_Redundancy);
            LabelsComponent = new[] { component.name };
            Formatter = value => value.ToString();
        }
        void Redundancy_Changed(object sender, SelectionChangedEventArgs args)
        {
            
            ComboBoxItem redundancy = ((sender as ComboBox).SelectedItem as ComboBoxItem);
            List<Component> components = db.Components.ToList();
            Component component = new Component();
            component = db.Components.Where(c => c.name == CurComponent && c.project_id == CurProject).Single();
            component.Redundancy = redundancy.Content.ToString();
            db.SaveChanges();

            if (component.Redundancy == "Без резервирования")
            {
                RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/No.png"));
                TextTotalTimeRedundancy.Text = TextTotalTime.Text;
                component.Total_Time_Redundancy = component.Total_Time;
                db.SaveChanges();

                if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                else
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));
            }
                
            else if (component.Redundancy == "Нагруженный")
            {
                RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Loaded.png"));
                double failureRate = Double.Parse(TextFailureRate.Text);
                int m = component.Multiplicity;
                double t = 100;

                double probability = 1 - Math.Pow(1 - Math.Pow(Math.E, (-1) * failureRate * t), (m + 1));
                while (probability > 0.999)
                { 
                    t = t + 100;
                    probability = 1 - Math.Pow(1 - Math.Pow(Math.E, (-1) * failureRate * t), (m + 1));
                }

                TextTotalTimeRedundancy.Text = Convert.ToString(t);

                component.Total_Time_Redundancy = (int)t;
                db.SaveChanges();

                if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                else
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));
            }
                
            else if (component.Redundancy == "Облегченный")
            {
                RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Light.png"));
                double failureRate = Double.Parse(TextFailureRate.Text);
                double failureRateStorage = Double.Parse(TextFailureRateStorage.Text);
                int m = component.Multiplicity;
                double t = 100;
                double alpha = failureRateStorage / failureRate;

                double probability = Math.Pow(Math.E, (-1) * failureRate * t) * (1 + (1 / alpha) * (1 - Math.Pow(Math.E, (-1) * alpha * failureRate * t)));

                while (probability > 0.999)
                {
                    t = t + 100;
                    probability = Math.Pow(Math.E, (-1) * failureRate * t) * (1 + (1 / alpha) * (1 - Math.Pow(Math.E, (-1) * alpha * failureRate * t)));
                }

                TextTotalTimeRedundancy.Text = Convert.ToString(t);

                component.Total_Time_Redundancy = (int)t;
                db.SaveChanges();

                if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                else
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));
            }
                
            else if (component.Redundancy == "Ненагруженный")
            {
                RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Unloaded.png"));
                double failureRate = Double.Parse(TextFailureRate.Text);
                double failureRateStorage = Double.Parse(TextFailureRateStorage.Text);
                int m = component.Multiplicity;
                double t = 100;
                double probability = 0;

                for (int j = 0; j < m+1; j++)
                {
                    probability = probability + (Math.Pow(failureRate * t, j) / factorial(j));
                }

                probability = probability * Math.Pow(Math.E, (-1) * failureRate * t);

                while (probability > 0.999)
                {
                    probability = 0;
                    t = t + 100;
                    for (int j = 0; j < m + 1; j++)
                    {
                        probability = probability + (Math.Pow(failureRate * t, j) / factorial(j));
                    }

                    probability = probability * Math.Pow(Math.E, (-1) * failureRate * t);
                }

                TextTotalTimeRedundancy.Text = Convert.ToString(t);

                component.Total_Time_Redundancy = (int)t;
                db.SaveChanges();

                if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                else
                    ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));
            }

            int valueTime = int.Parse(textTotalTime.Text);
            SeriesCollectionComponent[0].Values.Clear();
            if (component.Total_Time_Redundancy > valueTime)
                SeriesCollectionComponent[0].Values.Add(valueTime);
            else
                SeriesCollectionComponent[0].Values.Add(component.Total_Time_Redundancy);
            LabelsComponent = new[] { component.name };
            Formatter = value => value.ToString();
        }
        private void Button_ChooseWindow_Click(object sender, RoutedEventArgs e)
        {
            ChooseWindow chooseWindow = new ChooseWindow();
            if (this.WindowState == WindowState.Maximized)
            {
                chooseWindow.WindowState = WindowState.Maximized;
            }
            chooseWindow.Show();
            Close();
        }
        private void Button_Create_Component_Click(object sender, RoutedEventArgs e)
        {
            CreationComponentGrid.Visibility = Visibility.Visible;
            ComponentGrid.Visibility = Visibility.Collapsed;
            ProjectGrid.Visibility = Visibility.Collapsed;
            RedundancyGrid.Visibility = Visibility.Collapsed;
        }
        private void Button_Save_Component_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItem typeItem = (ComboBoxItem)ComboBoxType.SelectedItem;
            string type = "";

            string nominal = textBoxNominal.Text.Trim();
            string name = textBoxName.Text.Trim();
            string failure_rate = textBoxFailureRate.Text.Trim();
            string failure_rate_storage = textBoxFailureRateStorage.Text.Trim();
            string sample = textBoxSample.Text.Trim();

            if (ComboBoxType.SelectedItem != null)
            {
                type = typeItem.Content.ToString();
            }

            CleanToolTip();

            if (type.Length < 1)
            {
                UncorrectInput(ComboBoxType);
            }
            else if (nominal.Length < 1)
            {
                UncorrectInput(textBoxNominal);
            }
            else if (name.Length < 1)
            {
                UncorrectInput(textBoxName);
            }
            else if (!float.TryParse(failure_rate, out float failure_rateFloat))
            {
                UncorrectInput(textBoxFailureRate);
            }
            else if (!float.TryParse(failure_rate_storage, out float failure_rate_storageFloat))
            {
                UncorrectInput(textBoxFailureRateStorage);
            }
            else if (!int.TryParse(sample, out int sampleInt))
            {
                UncorrectInput(textBoxSample);
            }
            else
            {
                CleanToolTip();
                int multiplicity = 1;
                string redundancy = "Без резервирования";
                int total_time = 0;
                int total_time_redundancy = 0;

                Component component = new Component(sampleInt, failure_rateFloat, failure_rate_storageFloat, type, nominal, name, CurProject, redundancy, multiplicity, total_time, total_time_redundancy);

                db.Components.Add(component);
                db.SaveChanges();

                ProjectWindow projectWindow = new ProjectWindow(CurProject);
                if (this.WindowState == WindowState.Maximized)
                {
                    projectWindow.WindowState = WindowState.Maximized;
                }
                projectWindow.Show();
                Close();
            }

            void UncorrectInput(System.Windows.Controls.Control control)
            {
                control.Background = Brushes.Red;
            }
        }
        void CleanToolTip()
        {
            textBoxNominal.Background = Brushes.Transparent;
            textBoxName.Background = Brushes.Transparent;
            ComboBoxType.Background = Brushes.Transparent;
            textBoxFailureRate.Background = Brushes.Transparent;
            textBoxFailureRateStorage.Background = Brushes.Transparent;
            textBoxSample.Background = Brushes.Transparent;
        }
        private void Button_Component_Delete_Click(object sender, RoutedEventArgs e)
        {
            string projectId = CurProject;
            Button button = sender as Button;
            string componentName = (string)button.Content;
            Component component = new Component();

            component = db.Components.Where(c => c.name == componentName && c.project_id == projectId).Single();

            DeleteComponentWindow deleteWindow = new DeleteComponentWindow();

            if (deleteWindow.ShowDialog() == true)
            {
                db.Components.Remove(component);
                db.SaveChanges();
                ProjectWindow projectWindow = new ProjectWindow(CurProject);
                if (this.WindowState == WindowState.Maximized)
                {
                    projectWindow.WindowState = WindowState.Maximized;
                }
                projectWindow.Show();
                Close();
            }
        }
        private void Button_Cancel_Component_Click(object sender, RoutedEventArgs e)
        {
            CreationComponentGrid.Visibility = Visibility.Collapsed;
            ProjectGrid.Visibility = Visibility.Visible;
        }

        private void Button_Formula_Click(object sender, RoutedEventArgs e)
        {
            FormulaWindow formulaWindow = new FormulaWindow();


            if (formulaWindow.ShowDialog() == true)
            {
                Close();
            }
        }
        private void Button_Information_Click(object sender, RoutedEventArgs e)
        {
            InformationWindow informationWindow = new InformationWindow();


            if (informationWindow.ShowDialog() == true)
            {
                Close();
            }
        }
        private void Button_Project_Create_Click(object sender, RoutedEventArgs e)
        {
            ChooseWindow createWindow = new ChooseWindow();
            if (this.WindowState == WindowState.Maximized)
            {
                createWindow.WindowState = WindowState.Maximized;
            }
            createWindow.Show();
            createWindow.CreationProjectGrid.Visibility = Visibility.Visible;
            Close();
        }

        private void Button_Project_Delete_Click(object sender, RoutedEventArgs e)
        {
            Project project = new Project();
            project = db.Projects.Where(c => c.id == CurProject).Single();

            DeleteProjectWindow deleteWindow = new DeleteProjectWindow();

            if (deleteWindow.ShowDialog() == true)
            {

                db.Projects.Remove(project);
                db.SaveChanges();
                ChooseWindow createWindow = new ChooseWindow();
                if (this.WindowState == WindowState.Maximized)
                {
                    createWindow.WindowState = WindowState.Maximized;
                }
                createWindow.Show();
                Close();
            }
        }

        private void Button_Redundancy_Click(object sender, RoutedEventArgs e)
        {
            RedundancyGrid.Visibility = Visibility.Visible;
            CreationComponentGrid.Visibility = Visibility.Collapsed;
            ComponentGrid.Visibility = Visibility.Collapsed;
            ProjectGrid.Visibility = Visibility.Collapsed;

            List<Component> components = db.Components.ToList();
            List<RedundantComponent> redundantComponents = new List<RedundantComponent>(); 
            foreach (Component component in components)
                if (component.project_id == CurProject && component.Redundancy != "Без резервирования")
                {
                    redundantComponents.Add(new RedundantComponent() { Name = component.name, Type = component.Type, Redundancy = component.Redundancy, Multiplicity = component.Multiplicity });
                }
            RedundancyDataGrid.ItemsSource = redundantComponents;
        }

        public class RedundantComponent
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public string Redundancy { get; set; }
            public int Multiplicity { get; set; }
        }


        private void CartesianChart_DataClick(object sender, ChartPoint chartPoint)
        {

            ComponentGrid.Visibility = Visibility.Visible;
            CreationComponentGrid.Visibility = Visibility.Collapsed;
            ProjectGrid.Visibility = Visibility.Collapsed;
            RedundancyGrid.Visibility = Visibility.Collapsed;

            // Получение индекса точки данных
            int index = (int)chartPoint.X;
            string labelValue = "";
            // Проверка допустимости индекса
            if (index >= 0 && index < Labels.Length)
            {
                // Получение значения по индексу из Labels
                labelValue = Labels[index];
            }

            string componentName = labelValue;

            List<Component> components = db.Components.ToList();
            string projectId = CurProject;
            foreach (Component component in components)
                if (component.project_id == projectId && component.name == componentName)
                {
                    CurComponent = componentName;
                    Button_Project.Content = projectId;
                    TextBlockNominal.Text = component.Nominal.ToString();
                    TextType.Text = component.Type.ToString();
                    TextName.Text = component.name.ToString();
                    TextFailureRate.Text = component.Failure_rate.ToString();
                    TextFailureRateStorage.Text = component.Failure_rate_storage.ToString();
                    TextSample.Text = component.Sample.ToString();
                    TextMultiplicity.Text = component.Multiplicity.ToString();
                    RedundancyBox.Text = component.Redundancy;

                    double quantile = 0;
                    double gamma = (100 * component.Sample - 100) / component.Sample;
                    double gamma_percentile_life = -(Math.Log(gamma / 100) / component.Failure_rate);
                    double gamma_round = Math.Round(gamma);
                    double gamma_round1 = Math.Round(gamma, 1);
                    double gamma_round2 = Math.Round(gamma, 2);
                    var gamma_quantiles = new Dictionary<double, double>()
                    {
                        { 90, 1.282 },
                        { 91, 1.341 },
                        { 92, 1.405 },
                        { 93, 1.476 },
                        { 94, 1.555 },
                        { 95, 1.645 },
                        { 96, 1.751 },
                        { 97, 1.881 },
                        { 98, 2.054 },
                        { 99, 2.326 },
                        { 99.9, 3.090 },
                        { 99.99, 3.719 }
                    };

                    foreach (var gamma_quantile in gamma_quantiles)
                        if (gamma_quantile.Key == gamma_round)
                            quantile = gamma_quantile.Value;

                    foreach (var gamma_quantile in gamma_quantiles)
                        if (gamma_quantile.Key > gamma_round1 && gamma_round1 > 99)
                            quantile = gamma_quantile.Value;

                    foreach (var gamma_quantile in gamma_quantiles)
                        if (gamma_quantile.Key > gamma_round2 && gamma_round2 > 99.9)
                            quantile = gamma_quantile.Value;

                    TextTotalTime.Text = Convert.ToString(Math.Round((gamma_percentile_life * (1 - 0.15 * 3.090)) / (1 - 0.15 * quantile)));
                    component.Total_Time = int.Parse(TextTotalTime.Text);
                    db.SaveChanges();


                    TextTotalTimeRedundancy.Text = Convert.ToString(component.Total_Time_Redundancy);

                    if (component.Total_Time_Redundancy > Int32.Parse(textTotalTime.Text))
                        ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Success.png"));
                    else
                        ImageResult.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Failure.png"));

                    if (component.Redundancy == "Без резервирования")
                    {
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/No.png"));
                        TextTotalTimeRedundancy.Text = TextTotalTime.Text;
                        component.Total_Time_Redundancy = Convert.ToInt32(TextTotalTime.Text);
                        db.SaveChanges();
                    }

                    else if (component.Redundancy == "Нагруженный")
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Loaded.png"));
                    else if (component.Redundancy == "Облегченный")
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Light.png"));
                    else if (component.Redundancy == "Ненагруженный")
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Redundancy/Unloaded.png"));
                    else
                        RedundancyPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/noPhoto.png"));

                    if (component.Nominal == "IR2153")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/IR2153.jpg"));
                    else if (component.Nominal == "2N5401")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/2N5401.jpg"));
                    else if (component.Nominal == "2N5551")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/2N5551.jpg"));
                    else if (component.Nominal == "C1-4")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/C1-4.jpg"));
                    else if (component.Nominal == "FR607 x 4")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/FR607.jpg"));
                    else if (component.Nominal == "HER108")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/HER108.jpg"));
                    else if (component.Nominal == "IRF740")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/IRF740.jpg"));
                    else if (component.Nominal == "K10-17A")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/K10-17A.jpg"));
                    else if (component.Nominal == "K50-35")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/K50-35.jpeg"));
                    else if (component.Nominal == "K73-17")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/K73-17.jpg"));
                    else if (component.Nominal == "L15KC")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/L15KC.jpg"));
                    else if (component.Nominal == "L-132XIT")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/L-132XIT.jpg"));
                    else if (component.Nominal == "MIT-V")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/MIT-V.jpg"));
                    else if (component.Nominal == "PLA10")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/PLA10.jpg"));
                    else if (component.Nominal == "RS405L")
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/RS405L.jpg"));
                    else
                        ComponentPicture.Source = new BitmapImage(new Uri("pack://application:,,,/Pictures/Components/NoPhoto.jpg"));


                    int valueTime = int.Parse(textTotalTime.Text);
                    SeriesCollectionComponent[0].Values.Clear();
                    if (component.Total_Time_Redundancy > valueTime)
                        SeriesCollectionComponent[0].Values.Add(valueTime);
                    else
                        SeriesCollectionComponent[0].Values.Add(component.Total_Time_Redundancy);
                    ComponentAxisY.Labels = new[] { CurComponent };
                    Formatter = value => value.ToString();

                }
        }


            private void Button_MaxWindow_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
            }
            else this.WindowState = WindowState.Normal;
        }

        //Control Bar
        private void Drag(object sender, RoutedEventArgs e)
        {
            DragMove();
        }
        private void MinButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void MaxButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
            }
            else this.WindowState = WindowState.Normal;
        }
        private void ExitButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }




        #endregion
    }
}