﻿using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Color = System.Windows.Media.Color;

namespace AoD
{
    /// <summary>
    /// Логика взаимодействия для ChooseWindow.xaml
    /// </summary>
    public partial class ChooseWindow : Window
    {
        ApplicationContext db;

        public static ChooseWindow Instance;

        public ChooseWindow()
        {
            InitializeComponent();
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
            db = new ApplicationContext();

            List<Project> projects = db.Projects.ToList();
            List<string> listId = new List<string>();

            foreach (Project project in projects)
                listId.Add(project.id);

            Button[] buttons = new Button[listId.Count];
            Button[] delbuttons = new Button[listId.Count];

            for (int i = 0; i < listId.Count; i++)
            {
                buttons[i] = new Button();
                Style buttonStyle = (Style)FindResource("projectButton");
                buttons[i].Style = buttonStyle;
                buttons[i].Content = (listId[i]).ToString();
                buttons[i].Click += new RoutedEventHandler(Button_Project_Click);
                
                string b = listId[i].ToString();
   
                delbuttons[i] = new Button();
                delbuttons[i].Width = 30;
                delbuttons[i].HorizontalAlignment = HorizontalAlignment.Center;
                delbuttons[i].Margin = new Thickness(0, 5, 0, 12);
                delbuttons[i].Content = (listId[i]).ToString(); //
                delbuttons[i].Click += new RoutedEventHandler(Button_Delete_Click);
                delbuttons[i].BorderThickness = new Thickness(0);
                delbuttons[i].Background = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/Pictures/bin.png")));

            }

            AddToStackPanel(buttons);
            AddToStackPanelDel(delbuttons);
        }

        private void AddToStackPanel(Button[] buttons)
        {
            for (int i = 0; i < buttons.Length; i++)
                stackPanel.Children.Add(buttons[i]);
        }

        private void AddToStackPanelDel(Button[] buttons)
        {
            for (int i = 0; i < buttons.Length; i++)
                stackPanelDel.Children.Add(buttons[i]);
        }

        private void Button_Project_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            string curProject = (string)button.Content; ; //currentProject
            ProjectWindow projectWindow = new ProjectWindow(curProject);
            if (this.WindowState == WindowState.Maximized)
            {
                projectWindow.WindowState = WindowState.Maximized;
            }
            projectWindow.Show();
            Close();
        }

        private void Button_Create_Click(object sender, RoutedEventArgs e)
        {
            CreationProjectGrid.Visibility = Visibility.Visible;
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItem conditionItem = (ComboBoxItem)ComboBoxCondition.SelectedItem;
            ComboBoxItem equipmentItem = (ComboBoxItem)ComboBoxEquipmentGroup.SelectedItem;
            
            string id = textBoxProject.Text.Trim().ToUpper();
            string number = textBoxDecimalNumber.Text.Trim();
            string condition = "";
            string equipment = "";

            if (ComboBoxCondition.SelectedItem != null)
            {
                condition = conditionItem.Content.ToString();
            }

            if (ComboBoxEquipmentGroup.SelectedItem != null)
            {
                equipment = equipmentItem.Content.ToString();
            }


            string time = textBoxOperatingTime.Text.Trim();
            string temperature = textBoxTemperature.Text.Trim();

            CleanToolTip();

            if (id.Length < 1)
            {
                UncorrectInput(textBoxProject);
            }
            else if (number.Length < 1)
            {
                UncorrectInput(textBoxDecimalNumber);
            }
            else if (condition.Length < 1)
            {
                UncorrectInput(ComboBoxCondition);
            }
            else if (equipment.Length < 1)
            {
                UncorrectInput(ComboBoxEquipmentGroup);
            }
           
            else if (!int.TryParse(temperature, out int temperatureInt))
            {
                UncorrectInput(textBoxTemperature);
            }
            else if (!int.TryParse(time, out int timeInt))
            {
                UncorrectInput(textBoxOperatingTime);
            }
            else
            {
                CleanToolTip();

                //float failure_rate = 0;

                Project project= new Project(id, number, condition, equipment, timeInt, temperatureInt);

                db.Projects.Add(project);
                db.SaveChanges();

                ChooseWindow chooseWindow = new ChooseWindow();
                if (this.WindowState == WindowState.Maximized)
                {
                    chooseWindow.WindowState = WindowState.Maximized;
                }
                chooseWindow.Show();
                Close();
            }

            void UncorrectInput(System.Windows.Controls.Control control)
            {
                control.Background = Brushes.Red;
            }
        }

        void CleanToolTip()
        {
            textBoxProject.Background = Brushes.Transparent;
            textBoxDecimalNumber.Background = Brushes.Transparent;
            ComboBoxCondition.Background = Brushes.Transparent;
            ComboBoxEquipmentGroup.Background = Brushes.Transparent;
            textBoxOperatingTime.Background = Brushes.Transparent;
            textBoxTemperature.Background = Brushes.Transparent;
        }


        private void Button_Delete_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            string curProject = (string)button.Content;
            Project project = new Project();
            project = db.Projects.Where(c => c.id == curProject).Single();

            DeleteProjectWindow deleteWindow = new DeleteProjectWindow();

            if (deleteWindow.ShowDialog() == true)
            {

                db.Projects.Remove(project);
                db.SaveChanges();
                ChooseWindow createWindow = new ChooseWindow();
                if (this.WindowState == WindowState.Maximized)
                {
                    createWindow.WindowState = WindowState.Maximized;
                }
                createWindow.Show();
                Close();
            }
        }
        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
                CreationProjectGrid.Visibility = Visibility.Collapsed;
        }

        private void Button_Information_Click(object sender, RoutedEventArgs e)
        {
            InformationWindow informationWindow = new InformationWindow();


            if (informationWindow.ShowDialog() == true)
            {
                Close();
            }
        }



        //Control bar
        private void Drag(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
        private void MinButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void MaxButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
            } 
            else this.WindowState = WindowState.Normal;
        }
        private void ExitButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Image_MouseEnter(object sender, MouseEventArgs e)
        {

            DoubleAnimation animationImage = new DoubleAnimation
            {
                From = 1,
                To = 0.7,
                Duration = TimeSpan.FromSeconds(0.3)
            };

            DoubleAnimation animationButton = new DoubleAnimation
            {
                From = 0,
                To = 1,
                Duration = TimeSpan.FromSeconds(0.3)
            };


            MenuImage.BeginAnimation(Image.OpacityProperty, animationImage);
            MenuButton.BeginAnimation(Button.OpacityProperty, animationButton);
        }

        private void Image_MouseLeave(object sender, MouseEventArgs e)
        {

            DoubleAnimation animationImage = new DoubleAnimation
            {
                From = 0.7,
                To = 1,
                Duration = TimeSpan.FromSeconds(0.3)
            };

            DoubleAnimation animationButton = new DoubleAnimation
            {
                From = 1,
                To = 0,
                Duration = TimeSpan.FromSeconds(0.3)
            };


            MenuImage.BeginAnimation(Image.OpacityProperty, animationImage);
            MenuButton.BeginAnimation(Button.OpacityProperty, animationButton);
        }
    }
}